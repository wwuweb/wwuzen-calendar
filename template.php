<?php

/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 */

/**
 * Add wrappers around date components and format as "M j".
 */
function _date_add_component_wrappers($dateobject) {
  $month = $dateobject->format('M');
  $day_of_month = $dateobject->format('j');

  $month_output = '<span class="date-month">' . $month . '</span>';
  $day_of_month_output = '<span class="date-day-of-month">' . $day_of_month . '</span>';

  return $month_output . $day_of_month_output;
}

/**
 * Implements template_preprocess_date_display_single().
 */
function wwuzen_calendar_preprocess_date_display_single(&$variables) {
  if ($variables['dates']['format'] === 'M j') {
    $dateobject = $variables['dates']['value']['local']['object'];
    $variables['date'] = _date_add_component_wrappers($dateobject);
  }
}

/**
 * Implements template_preprocess_date_display_range().
 */
function wwuzen_calendar_preprocess_date_display_range(&$variables) {
  if ($variables['dates']['format'] === 'M j') {
    $dateobject = $variables['dates']['value']['local']['object'];
    $dateobject2 = $variables['dates']['value2']['local']['object'];
    $variables['date1'] = _date_add_component_wrappers($dateobject);
    $variables['date2'] = _date_add_component_wrappers($dateobject2);
  }
}

//To support the metatag schema module -
// https://www.drupal.org/project/zen/issues/2934644
function wwuzen_calendar_process_html_tag(&$variables) {
  $tag = &$variables['element'];

  if ($tag['#tag'] == 'style' || $tag['#tag'] == 'script') {
    // Remove redundant CDATA comments.
    unset($tag['#value_prefix'], $tag['#value_suffix']);

    // Remove redundant type attribute.
    if (isset($tag['#attributes']['type'])) {
      if ($tag['#attributes']['type'] !== 'text/ng-template'
          && $tag['#attributes']['type'] !== 'application/ld+json') {
        unset($tag['#attributes']['type']);
      }
    }

    // Remove media="all" but leave others unaffected.
    if (isset($tag['#attributes']['media']) && $tag['#attributes']['media'] === 'all') {
      unset($tag['#attributes']['media']);
    }
  }
}
