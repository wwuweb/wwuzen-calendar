<?php

/**
 * @file
 * Page template.
 */
?>
<div class="page">
  <header role="banner">
    <div class="center-content">
      <section class="western-header" aria-label="University Links, Search, and Navigation">
        <span class="western-logo"><a href="http://www.wwu.edu">Western Washington University</a></span>
        <nav aria-label="University related">
          <div class="western-header-nav-group">
            <div class="western-quick-links" aria-label="Western Quick Links">
              <button aria-pressed="false">Toggle Quick Links</button>
              <ul>
                <li><a href="http://www.wwu.edu/academic_calendar" title="Calendar"><span aria-hidden="true">c</span> <span>Calendar</span></a></li>
                <li><a href="http://www.wwu.edu/directory" title="Directory"><span aria-hidden="true">d</span> <span>Directory</span></a></li>
                <li><a href="http://www.wwu.edu/index" title="Index"><span aria-hidden="true">i</span> <span>Index</span></a></li>
                <li><a href="http://www.wwu.edu/campusmaps" title="Map"><span aria-hidden="true">l</span> <span>Map</span></a></li>
                <li><a href="http://mywestern.wwu.edu" title="myWestern"><span aria-hidden="true">w</span> <span>myWestern</span></a></li>
              </ul>
            </div>
            <div class="western-search" role="search" aria-label="University and Site">
              <button aria-pressed="false">Open the search box</button>
              <div class="western-search-widget">
                <?php print $search_box; ?>
              </div>
            </div>
          </div>
          <button class="mobile-main-nav" aria-pressed="false">Open Main Navigation</button>
        </nav>
      </section>
      <section class="site-header" aria-label="Site Header">
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
          <?php if ($logo): ?>
          <div class="site-banner"><img src="<?php print $logo;?>" alt=""></div>
          <?php endif; ?>
          <div class="site-name">
            <?php if ($site_name): ?>
            <p><span><?php print $site_name; ?></span></p>
            <?php endif; ?>
          </div>
        </a>
      </section>
    </div>
    <nav class="main-nav" id="main-menu" aria-label="Main">
      <?php print render($page['navigation']); ?>
    </nav>
    <?php print render($page['header']); ?>
  </header>
  <main>
    <header class="page-title">
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
      <h1><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print $breadcrumb; ?>
    </header>
    <section class="content column">
      <?php print render($page['highlighted']); ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php print render($tabs); ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
      <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
    </section>
    <?php if ($secondary_menu): ?>
    <nav class="secondary-nav" aria-label="Secondary">
      <?php
        print theme('links__system_secondary_menu', array(
          'links' => $secondary_menu,
          'attributes' => array(
            'class' => array('links', 'inline', 'clearfix'),
          ),
          'heading' => array(
            'text' => $secondary_menu_heading,
            'level' => 'h2',
            'class' => array('element-invisible'),
          ),
        ));
      ?>
    </nav>
    <?php endif; ?>
    <?php $sidebar_first = render($page['sidebar_first']); ?>
    <?php $sidebar_second = render($page['sidebar_second']); ?>
    <?php if ($sidebar_first || $sidebar_second): ?>
    <aside class="content-sidebar">
      <?php print $sidebar_first; ?>
      <?php print $sidebar_second; ?>
    </aside>
    <?php endif; ?>
  </main>
</div> <!-- end div.page -->
<footer role="contentinfo">
  <div class="footer-wrapper">
    <div class="footer-left">
      <?php print render($page['footer_left']); ?>
    </div>
    <div class="footer-center">
      <?php print render($page['footer_center']); ?>
      <div class="western-privacy-statement">
        <a href="http://www.wwu.edu/privacy/">Website Privacy Statement</a>
        <a href="https://www.wwu.edu/commitment-accessibility">Accessibility</a>
      </div>
    </div>
    <div class="footer-right" role="complementary" aria-label="WWU contact info">
      <h2><a href="http://www.wwu.edu">Western Washington University</a></h2>
      <div class="western-contact-info">
            <p><span aria-hidden="true" class="western-address"></span>516 High Street<br>
              <span class="western-address-city">Bellingham, WA 98225</span></p>
            <p><span aria-hidden="true" class="western-telephone"></span><a href="tel:3606503000">(360) 650-3000</a></p>
            <p><span aria-hidden="true" class="western-contact"></span><a href="http://www.wwu.edu/wwucontact/">Contact Western</a></p>
          </div>
          <div class="western-social-media">
            <ul>
              <li><a href="https://social.wwu.edu"><span aria-hidden="true" class="westernIcons-WwuSocialIcon"></span>Western Social</a></li>
              <li><a href="http://www.facebook.com/westernwashingtonuniversity"><span aria-hidden="true" class="westernIcons-FacebookIcon"></span>WWU Facebook</a></li>
              <li><a href="http://www.flickr.com/wwu"><span aria-hidden="true" class="westernIcons-FlickrIcon"></span>WWU Flickr</a></li>
              <li><a href="http://www.youtube.com/wwu"><span aria-hidden="true" class="westernIcons-YouTubeIcon"></span>WWU Youtube</a></li>
              <li><a href="http://news.wwu.edu/go/feed/1538/ru/atom/"><span aria-hidden="true" class="westernIcons-RSSicon"></span>Western Today RSS</a></li>
            </ul>
          </div>
      <?php print render($page['footer_right']); ?>
    </div>
  </div> <!-- end div.footer-wrapper -->
</footer>
<?php print render($page['bottom']); ?>
